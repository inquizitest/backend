package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/inquizitest/backend/pkg/auth"
	"gitlab.com/inquizitest/backend/pkg/websocket"
)

var addr = flag.String("addr", ":3000", "http service address")

func main() {

	var tokenList []auth.Token
	flag.Parse()
	hub := websocket.NewHub(tokenList)
	go hub.Run()

	r := mux.NewRouter()
	r.HandleFunc("/", websocket.ServeHome)
	r.HandleFunc("/login", auth.Login(tokenList)).Methods("POST")
	r.HandleFunc("/ws", websocket.ServeWs(hub, tokenList))

	http.Handle("/", r)

	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
