package auth

import (
	"encoding/json"
	"log"
	"net/http"

	qjson "gitlab.com/inquizitest/backend/pkg/json"
)

type Credentials struct {
	Name string `json:"name"`
	Pass string `json:"pass"`
}

type Student struct {
	ID       int    `json:"id"`
	Grade    int    `json:"grade"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type Response struct {
	Token string `json:"token"`
}

func Login(tokenList []Token) func(w http.ResponseWriter, r *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {
		creds := Credentials{}

		if err := json.NewDecoder(r.Body).Decode(&creds); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		student := studentExists(&creds)
		if student == nil {
			http.Error(w, "Wrong login details", http.StatusUnauthorized)
			return
		}

		token, err := NewToken(student)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		tokenList = append(tokenList, *token)

		resp := &Response{
			Token: token.Token.String(),
		}

		respMessage, err := json.Marshal(resp)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.WriteHeader(http.StatusOK)
		w.Write(respMessage)
	}

}

func studentExists(creds *Credentials) *Student {
	studentList, err := qjson.ReadJSONFile("auth/students.json")
	if err != nil {
		log.Fatal(err)
	}

	var students []Student

	json.Unmarshal(studentList, &students)

	for _, stud := range students {
		if stud.Name == creds.Name && stud.Password == creds.Pass {
			return &stud
		}
	}

	return nil
}
