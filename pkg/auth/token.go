package auth

import (
	"github.com/google/uuid"
)

type Token struct {
	Token   uuid.UUID
	student *Student
}

func NewToken(student *Student) (*Token, error) {
	token := &Token{}

	newUUID, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}
	token.Token = newUUID
	token.student = student

	return token, nil
}
