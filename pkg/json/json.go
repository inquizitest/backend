package json

import (
	"encoding/json"
	"io/ioutil"
)

// ReadJSONFile read the fileName file and checks if the file is actually JSON
func ReadJSONFile(fileName string) ([]byte, error) {

	jsonFile, err := ioutil.ReadFile(fileName)
	// if err != nil || !isJSON(jsonFile) {
	if err != nil {
		return nil, err
	}

	return jsonFile, nil
}

func isJSON(v []byte) bool {

	var js map[string]interface{}

	return json.Unmarshal(v, &js) == nil
}
