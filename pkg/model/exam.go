package model

import (
	"encoding/json"

	qjson "gitlab.com/inquizitest/backend/pkg/json"
)

// NewExam return an empty exam struct
func NewExam() Exam {
	return &exam{}
}

// Exam is an interface for an exam containing the exam subject and questions
// Fill the implemented struct with an exam.json
type Exam interface {
	GetSubject() string
	GetQuestions() []Question
}

type exam struct {
	Subject   string     `json:"subject"`
	Questions []Question `json:"questions"`
}

type Question struct {
	QuestionID int    `json:"questionID"`
	Question   string `json:"question"`
	Time       int    `json:"time"`
}

func (e *exam) GetSubject() string {
	return e.Subject
}

func (e *exam) GetQuestions() []Question {
	return e.Questions
}

// LoadExam loads the exam in the fileName json file into a golang struct
func LoadExam(fileName string) (Exam, error) {
	jsonExam, err := qjson.ReadJSONFile(fileName)
	if err != nil {
		return nil, err
	}

	exam := &exam{}
	populateExam(exam, jsonExam)

	return exam, nil
}

func populateExam(emptyExam Exam, examJSON []byte) error {

	if err := json.Unmarshal(examJSON, emptyExam); err != nil {
		return err
	}

	return nil
}
