package websocket

import (
	"log"
	"net/http"

	"gitlab.com/inquizitest/backend/pkg/auth"
	"gitlab.com/inquizitest/backend/pkg/model"
)

// ServeHome serves the html.. will remove soon
func ServeHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "home.html")
}

// ServeWs handles websocket requests from the peer.
func ServeWs(hub *Hub, tokenList []auth.Token) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		tokenFromHeader := r.Header.Get("x-api-token")
		if tokenFromHeader == "" {
			http.Error(w, "Did not receive a token", http.StatusUnauthorized)
			return
		}

		authorized := false
		var clientToken *auth.Token
		for _, token := range tokenList {
			if token.Token.String() == tokenFromHeader {
				authorized = true
				clientToken = &token
				break
			}
		}

		if !authorized {
			http.Error(w, "Token does not exist", http.StatusUnauthorized)
			return
		}

		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}
		client := &Client{hub: hub, token: clientToken, conn: conn, send: make(chan []byte, 256)}
		client.hub.register <- client

		// Allow collection of memory referenced by the caller by doing all work in
		// new goroutines.
		// go client.writePump()

		loadedExam, err := model.LoadExam("exams/math_g7.json")
		if err != nil {
			log.Fatal(err)
		}

		go client.sendExamQuestions(loadedExam)
		go client.writePump()
		go client.readPump()
	}
}
